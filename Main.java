import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Main {
    public static void count_days(List<LocalDate> aux){
        System.out.println("(Task 1) Number of distinct days:" + aux.stream().distinct().count());
    }
    public static Map<String,Integer> count_activities(List<String> aux){
        Map<String,Integer> rez=new HashMap<String, Integer>() ;
        int [] cnt={0};
        int[] ok={1};
        aux.stream().forEach(x->{
            aux.stream().forEach(x1->{
                if(x.equals(x1)){
                    cnt[0]++;
                }
            });
            rez.entrySet().stream().forEach(x1->{
                if(x1.equals(x)){
                    ok[0]=0;
                }
            });
            if(ok[0]==1){
                rez.put(x,cnt[0]);
            }
            ok[0]=1;
            cnt[0]=0;
        });

        return rez;
    }
    public static Map<Integer,Map<String,Integer>> count_ac_per_day(List<LocalDate> aux,List<String> aux1){
        Map<Integer,Map<String,Integer>> rez=new HashMap<>();
        int [] i={0};
        int[] ok={1};
        int[] cnt={0};

        aux.stream().forEach(x->{
            Map<String,Integer> test=new HashMap<>();
            int current_day=x.getDayOfMonth();
            aux.stream().forEach(x1->{
                if(x1.getDayOfMonth()==current_day){
                    String ac=aux1.get(aux.indexOf(x1));
                    test.entrySet().stream().forEach(x2->{
                        if(x2.equals(ac)){
                            ok[0]=0;
                            int val=test.get(ac);
                            test.remove(ac);
                            test.put(ac,val++);
                        }
                    });
                    if(ok[0]==1){
                        test.put(ac,cnt[0]++);
                    }
                }
            });
            cnt[0]=0;
            ok[0]=1;
           // int date=Integer.parseInt(rez.keySet().toString());
            rez.keySet().stream().forEach(x3->{
                if(current_day==x3){
                    ok[0]=0;
                }
            });
            if(ok[0]==1){
                rez.put(current_day,test);
            }
            else{
                rez.remove(current_day);
                rez.put(current_day,test);
            }
            ok[0]=1;
        });

        return rez;
    }
    public static Map<String, Duration> duration(List<MonitoredData> ob,List<String> ac,List<LocalTime> t1,List<LocalTime> t2){
        Map<String,Duration> rez=new HashMap<>();
        LocalTime r1=LocalTime.of(0,0,0);
        Duration f=Duration.ZERO;
        Duration[] fin={f};
        int[] h={0},m={0},s={0};
        ac.stream().forEach(x->{
            ob.stream().forEach(x2->{
                if(x2.getActivity().equals(x)){

                    LocalTime st=LocalTime.of(0,0,0);
                    LocalTime et=LocalTime.of(0,0,0);
                    et=t2.get(ob.indexOf(x2));
                    st=t1.get(ob.indexOf(x2));
                    Duration d=Duration.between(et,st);
                    fin[0]=fin[0].plus(d);
                }
            });


            rez.put(x,fin[0]);
            fin[0]=Duration.ZERO;
        });

        return rez;
    }
    public static List<String> count_percent(Map<String,Integer> m,List<MonitoredData> list,List<LocalTime> t1,List<LocalTime> t2){
        List<String> rez=new ArrayList<>();
        int [] cnt={0};

        m.keySet().stream().forEach(x->{
            list.stream().forEach(x1->{
                if(x1.getActivity().equals(x)){
                    LocalTime st=LocalTime.of(0,0,0);
                    LocalTime et=LocalTime.of(0,0,0);
                    et=t2.get(list.indexOf(x1));
                    st=t1.get(list.indexOf(x1));
                    Duration d=Duration.between(st,et);
                    Duration d1=Duration.ofMinutes(5);
                    int minutes = (int) ((d.getSeconds() % (60 * 60)) / 60);
                    if(minutes<5){
                        cnt[0]++;
                    }
                }
            });
            double proc=(90.0/100)*m.get(x);
            if(Double.valueOf(cnt[0])>proc){
                rez.add(x);
            }
            int k=0;
            cnt[0]=new Integer(0);
        });
        return rez;
    }
    public static void main(String[] args) throws IOException {
        ArrayList<MonitoredData> list=new ArrayList<MonitoredData>();
        Stream<String> str= Files.lines(Paths.get("Activities.txt"));
        str.map(line->line.split("\t\t")).map(x->new MonitoredData(x[0].trim(),x[1].trim(),x[2].trim())).map(x->list.add(x)).collect(Collectors.toList());
        List<LocalDate> aux=new ArrayList<LocalDate>() ;
        List<String> aux1=new ArrayList<>();
        list.stream().map(x->x.getStart_time().split(" ")).map(x->LocalDate.parse(x[0])).map(x->aux.add(x)).collect(Collectors.toList());
        count_days(aux);
        list.stream().map(x->x.getActivity().trim()).map(x->new String(x)).map(x->aux1.add(x)).collect(Collectors.toList());
        System.out.println("\n(Task 2) Number of each activity:");
        count_activities(aux1).entrySet().stream().forEach(x-> System.out.println(x));
        System.out.println("\nTask3: ");
        count_ac_per_day(aux,aux1).entrySet().stream().forEach(x-> System.out.println(x));
        List<String> act=new ArrayList<>();
        count_activities(aux1).keySet().stream().forEach(x->{
            act.add(x.trim());
        });

        System.out.println();
        List<LocalTime> t1=new ArrayList<>();
        List<LocalTime> t2=new ArrayList<>();
        list.stream().map(x->x.getStart_time().split(" ")).map(x->LocalTime.parse(x[1])).map(x->t1.add(x)).collect(Collectors.toList());
        list.stream().map(x->x.getEnd_time().split(" ")).map(x->LocalTime.parse(x[1])).map(x->t2.add(x)).collect(Collectors.toList());

        System.out.println("\nTask 5:");
        duration(list,act,t1,t2).entrySet().stream().forEach(x-> System.out.println(x));


        System.out.println("\nTask 6:");
        count_percent(count_activities(aux1),list,t1,t2).stream().forEach(x-> System.out.println(x));

    }
}
