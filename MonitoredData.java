public class MonitoredData {
    private String start_time;
    private String end_time;
    private String Activity;

    public MonitoredData(String start_time, String end_time, String activity) {
        this.start_time = start_time;
        this.end_time = end_time;
        Activity = activity;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getActivity() {
        return Activity;
    }

    public void setActivity(String activity) {
        Activity = activity;
    }
}
